![inline](logo.png)

# *Everything Open*

---

## *Job van der Voort*
### VP of Product at GitLab
### @Jobvo

---

# [fit] *__History of GitLab__*

---

#[fit] 2011: Open Source
### *Started by Dmitriy, who didn't have running water*

---

#[fit] 2013: GitLab.com
### *Founded by Sytse*
#### (already had running water)

---

> I want to work on GitLab full time
-- Dmitriy, still without running water

---

#[fit] 2014: Founding of GitLab BV
### *5 engineers, 1 sales*

![](founding.jpg)

---

### 2015
# *Y Combinator*
![](team_yc.jpg)

---

#[fit] _seed + a round_
### Khosla Ventures; Ashton Kutcher; Michael Dell

---

## _> 150,000_ organisations
## 1000 contributors

---

## *~50 employees*

![](team_now.jpg)

---

### Paying customers
##  _NASA, IBM, SPACEX, AIRBNB, SONY, ESA, CERN, alibaba_

---

#[fit] *Open GitLab*

---

# Open core

---

![inline](open core.png)

---

![inline](open core 2.png)

---

## *Community of hundreds of developers works on Community Edition*

---

## _Enterprise Edition_ is for _enterprises_

---

## *GitLab Inc works on both*
### All is good
#### __Dmitriy still doesn't have running water__

---

## __*however*__
<!-- we see a trend, have a realisation -->

---

### we realized that
## _everyone starts by using the community edition_

---

## _it is not organizations that become our customers_

---

## _it's the community_

---

## __*what we did*__

---

# Opened everything

---

### _Support, Development, Operations, Organization, Direction, Strategy, Planning_

---

## __*From support to fix*__

---

#### __*From support to fix*__

## Zendesk -> GitLab.com

---

#### __*From support to fix*__


```
- zendesk issue: <link>

A big consumer electronics company reports
that when pressing a button, things break.
```

---

#### __*From support to fix*__

![inline](support.png)

---

#### __*From support to fix*__

Fix is scheduled for a release*

A developer picks up the issue

Logs status and links merge request

The fix is merged by a senior developer
(can be outside of GitLab Inc)

---

### *often, a fix is contributed before we can start to work on it*

---

## __*Feature Requests*__

---

#### __*Feature Requests*__

### Issues on the same open issue tracker on GitLab.com

---

#### __*Feature Requests*__

### Everyone can vote, comment and contribute

---

#### __*Feature Requests*__

![inline](feature_proposal.png)

---

#### __*Feature Requests*__

Feature is scheduled for a release*

A developer works on the feature

Logs status and links merge request

The feature is merged

---

> Everyone a part of the community
-- Employees, customers, hobbyists, organizations

---

## Everyone has the same tools

---

## _REMOTE-FIRST_
![](team.png)

---

## __*How to build a product from thousands of voices*__

---

##[fit] _Schedule issues for a release_
### E.g. Notification Center in 8.5

---

##[fit] _Release every month on the 22nd_
### since september 2011

---

##[Fit] _Build iteratively_
### Implement the minimally viable feature and iterate based on feedback

---

##[Fit] _Listen_
### The thousands of voices have thousands of cool ideas. Use them.

---

## _Plan_
### about.gitlab.com/direction

---

## __*How to collaborate within a community*__

---

## For all cool stuff
##[fit] _[ Accepting Merge Requests ]_

---

### To make the barrier of entry low
## _[ up-for-grabs ]_

---

## __*What is hard?*__

---

# Saying no

---

#[fit] __*Other things open*__

---

#[fit] _Open Organization_
### about.gitlab.com/handbook
![fit right](handbook.png)

---

#[fit] _Open Culture_
### about.gitlab.com/culture
![fit left](culture.png)

---

#[fit] _Open Strategy_
### about.gitlab.com/strategy

---

#[fit] _Open Operations_
### gitlab.com/gitlab-com/operations

---

### Collaborate
## _Be open_

---

# _Questions?_
### @Jobvo
